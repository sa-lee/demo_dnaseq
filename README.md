# Demo analysis of DNA-seq data for P.falciparum

Here we use the Malaria Genomics consortium data to demo an analysis of 
high-throughput sequencing data. The text file PRJEB2136.txt was
downloaded from http://www.ebi.ac.uk/ena/data/view/PRJEB2136
and contains the ftp links for interrogating the data. 
The correspondence betweeen accession numbers and sample identifiers is
avaialble at https://www.malariagen.net/sites/default/files/content/data-set/file/PfSampleData_MalariaGENPfCP_Jan2016_v1.pdf 

Note that a lot of the information here can be found at many other sources.

We recommend becoming familiar with the unix system before embarking on doing
bioinformatics analyses. Think of this like a bioinformatician’s pipette - without being able to properly use it we can’t do any experiements! An excellent introduction is available here:
http://korflab.ucdavis.edu/unix_and_Perl/

We’ll go through some examples today.

For example, reading over the tutorials here
https://software.broadinstitute.org/gatk/guide/topic?name=tutorials
or  http://ged.msu.edu/angus/tutorials/ are excellent places to start.

## Downloading a test sample

To begin we download a single PNG isolate (PN0008-C) as paired-end fastq files.

This line searches for the isolate we want to download then 
extracts the links to retrieve the fastq files and then downloads
them in parallel. Then we move them to our fastq directory.
```{bash}
grep -e ERS150810 PRJEB2136.txt | cut -f11 | tr ';' '\n' | parallel 'wget -i ftp://{}'

mv *.fastq.gz fastqc/
```

## Beginning analysis

As a first look at our data we can extract QC metrics using fastqc
for our paired-end reads.
```{bash}
mkdir qc
fastqc fastq/ERR175543*.fastq.gz —o qc
```

This produces a HTML report for each fastq file and a zip archive containing more detailed results. Take a look at the reports produced for our reads.

The details of what each module means is available here:
http://www.bioinformatics.bbsrc.ac.uk/projects/fastqc/Help/

Another tutorial for using FastQC is [here](http://ged.msu.edu/angus/tutorials-2011/fastq_tutorial.html)

Another step after performing FastQC would be to trim or soft-clip subsequences within reads that contain adapter sequence. One way to do this is to convert the fastq files to an unmapped BAM file and then mark adapters using picard tools. 

```{bash}
module load picard-tools 
# generate uBAM (could discard fastq files at this point)
FastqToSam F1=fastq/ERR175543_1.fastq.gz F2=fastq/ERR175543_2.fastq.gz O=ERR175543.bam SM=PN0008_C  SORT_ORDER=queryname 


MarkIlluminaAdapters I=ERR175543.bam O=ERR175543.markadapters.bam M=ERR175543_markadapters_report.txt
```


## Alignment 

Next we can perform alignment using our aligner of choice. For this we require a reference genome, and an index. Here we use bwa, however there are many other options available.

This step is slightly complicated so we’ll go through it step by step, then put it together.

We have prepared a concatenated reference genome consisting of 
Pf3D7, PvivaxP01, and hg19. (We have also created the relevant indexes for bwa and for use in other downstream tools such as PicardTools. 

To create the indexes we type the following

```{bash}
module load bwa
bwa index -p Pf3D7_11.1_PvivaxP01_hg19substplus_gatk Pf3D7_11.1_PvivaxP01_hg19substplus_gatk.fasta 
```


Then we can start the alignment process.


```{bash}
INDEX=fasta/Pf3D7_11.1_PvivaxP01_hg19substplus_gatk 
REF=fasta/Pf3D7_11.1_PvivaxP01_hg19substplus_gatk.fasta
TMPDIR=/usr/local/work/

SamToFastq I=ERR175543.markadapters.bam FASTQ=/dev/stdout \
    CLIPPING_ATTRIBUTE=XT CLIPPING_ACTION=2 INTERLEAVE=true NON_PF=true \
    TMP_DIR=$TMPDIR | \
    bwa mem -M -t 4 -p $INDEX /dev/stdin | \
    MergeBamAlignment ALIGNED_BAM=/dev/stdin \
       UNMAPPED_BAM=ERR175543.bam \
       OUTPUT=ERR175543.aligned.bam \
       R=$REF CREATE_INDEX=true ADD_MATE_CIGAR=true \
       CLIP_ADAPTERS=false CLIP_OVERLAPPING_READS=true \
       INCLUDE_SECONDARY_ALIGNMENTS=true MAX_INSERTIONS_OR_DELETIONS=-1 \
       PRIMARY_ALIGNMENT_STRATEGY=MostDistant ATTRIBUTES_TO_RETAIN=XS \
       TMP_DIR=$TMPDIR
```

What does each step do?

The first command creates interleaved fastq files and soft clips bases in adapter sequences to have very low base-quality scores (so they are discarded by the alignment algorithm).
The output is then piped into bwa mem (with the mark secondary hits option set and running on four CPU threads).
This command will map the reads to our concatenated reference genome.

The mapped reads are then piped in to the MergeBamAlignment tool, so we can store the mapped reads and the raw reads in a single file. We also add a CIGAR tag to the paired end reads that are mates, so we have alignment quality scores on the reads.
We also maintain any secondary alignment information, clipping overlapping reads,and allow any number of insertions/deletions to remain.


## Post processing reads

To assess the quality of our mapping there are several checks we can do:

1. Make alignment summary
2. Rerun fastqc on the aligned BAM
3. Check insert size distributions (https://www.biostars.org/p/95803/; http://thegenomefactory.blogspot.com.au/2013/08/paired-end-read-confusion-library.html)
4. Check GC content

To do 1. and 3., 4. we can use Picard Tools.

```{bash}
CollectAlignmentSummaryMetrics R=$REF \
    INPUT=ERR175543.aligned.bam \
    OUTPUT=alignment_summary.txt

CollectInsertSizeMetrics \
      I=ERR175543.aligned.bam \
      O=insert_size_metrics.txt \
      H=insert_size_histogram.pdf \
      M=0.5

CollectGcBiasMetrics R=$REF \
    INPUT=ERR175543.aligned.bam \
    OUTPUT=gc_bias_summary.txt \
    CHART_OUTPUT=gc_check.pdf \
    SUMMARY_OUTPUT=summary.txt

```


You’re now ready for to interrogate your data further! 

Other useful tools:

samtools - http://www.htslib.org

bwa - http://bio-bwa.sourceforge.net/bwa.shtml

deeptools - https://deeptools.github.io

picard-tools - https://broadinstitute.github.io/picard/index.html



